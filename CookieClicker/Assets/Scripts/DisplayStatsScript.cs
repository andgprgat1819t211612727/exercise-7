﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayStatsScript : MonoBehaviour
{
    public static int autoClickerLvl;
    public Text autoClickerLevel;
    public static int cookieScore;
    public Text cookies;
    public static int grandmaLvl;
    public Text grandmaLevel;
    public static int grandmaLvlCost;
    public Text grandmaCost;
    public static int autoClickerLvlCost;
    public Text autoClickerCost;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        autoClickerLevel.text = "AutoClickerLevel : " + autoClickerLvl;
        grandmaLevel.text = "GrandmaLevel : " + grandmaLvl;
        cookies.text = "Cookies : " + cookieScore;
        grandmaCost.text = "Upgrade Cost : " + grandmaLvlCost;
        autoClickerCost.text = "Upgrade Cost : " + autoClickerLvlCost;
    }
}
