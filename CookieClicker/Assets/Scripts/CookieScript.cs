﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CookieScript : MonoBehaviour
{
    public SaveUtility saveUtil;
    public int cookies = 0;
    public int grandmaLvl = 0;
    public int grandmaCost = 100;
    public int autoClickerLvl = 0;
    public int autoClickerCost = 100;
    public float second = 0;
    // Start is called before the first frame update
    void Start()
    {
        saveUtil.loadStats();
    }

    // Update is called once per frame
    void Update()
    {
        updateDisplayStats();
        if (Input.GetKeyDown(KeyCode.Backspace))
        {
            saveUtil.resetStats();
        }
        if(second>=1)
        {
            cookiePerSecond();
            second = 0;
        }
        second += Time.deltaTime;
        print("Cookies" + cookies);

        saveUtil.saveScore();
    }
    private void OnMouseUp()
    {
        cookies += 1;
    }

    public void cookiePerSecond()
    {
        cookies += grandmaLvl * 1;
        cookies += autoClickerLvl * 1;
    }

    public void upgradeGrandma()
    {
        if (cookies >= grandmaCost)
        {
            cookies -= grandmaCost;
            grandmaLvl += 1;
            grandmaCost *= 2;
        }
    }

    public void upgradeAutoClicker()
    {
        if (cookies >= autoClickerCost)
        {
            cookies -= autoClickerCost;
            autoClickerLvl += 1;
            autoClickerCost *= 2;
        }
    }

    private void updateDisplayStats()
    {
        DisplayStatsScript.cookieScore = cookies;
        DisplayStatsScript.grandmaLvl = grandmaLvl;
        DisplayStatsScript.autoClickerLvl = autoClickerLvl;
        DisplayStatsScript.grandmaLvlCost = grandmaCost;
        DisplayStatsScript.autoClickerLvlCost = autoClickerCost;
    }
}
