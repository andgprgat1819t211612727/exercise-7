﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CookieScoreScript 
{
    public int cookieScore;
    public int autoClickerLevel;
    public int grandmaLevel;
    public int grandmaCost;
    public int autoClickerCost;
}
