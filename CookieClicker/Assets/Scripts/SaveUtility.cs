﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveUtility : MonoBehaviour
{
    public Ball ball;
    public CookieScript cookieGame;

    public void savePosition()
    {
        BallPosition ballPos = new BallPosition();
        ballPos.x = ball.transform.position.x;
        ballPos.y = ball.transform.position.y;
        ballPos.z = ball.transform.position.z;

        string jsonString = JsonUtility.ToJson(ballPos);
        print(jsonString);

        PlayerPrefs.SetString("BallPos", jsonString);
    }

    public void saveScore()
    {
        CookieScoreScript cookie = new CookieScoreScript();
        cookie.cookieScore = cookieGame.cookies;
        cookie.grandmaLevel = cookieGame.grandmaLvl;
        cookie.autoClickerLevel = cookieGame.autoClickerLvl;
        cookie.grandmaCost = cookieGame.grandmaCost;
        cookie.autoClickerCost = cookieGame.autoClickerCost;

        string jsonString = JsonUtility.ToJson(cookie);
        print(jsonString);

        PlayerPrefs.SetString("CookieStats", jsonString);
    }
    public void resetStats()
    {
        CookieScoreScript cookie = new CookieScoreScript();
        cookie.cookieScore = 0;
        cookie.grandmaLevel = 0;
        cookie.autoClickerLevel = 0;
        cookie.grandmaCost = 100;
        cookie.autoClickerCost = 100;

        string jsonString = JsonUtility.ToJson(cookie);
        print(jsonString);

        PlayerPrefs.SetString("CookieStats", jsonString);
    }

    public void loadPosition()
    {
        if (PlayerPrefs.HasKey("BallPos"))
        {
            string jsonStrng = PlayerPrefs.GetString("BallPos");
        
            BallPosition ballPost = JsonUtility.FromJson<BallPosition>(jsonStrng);
            if (ballPost != null)
            {
                ball.transform.position = new Vector3(ballPost.x, ballPost.y, ballPost.z);
            }
        }
    }

    public void loadStats()
    {
        if (PlayerPrefs.HasKey("CookieStats"))
        {
            string jsonStrng = PlayerPrefs.GetString("CookieStats");

            CookieScoreScript cookieStats = JsonUtility.FromJson<CookieScoreScript>(jsonStrng);
            if (cookieStats != null)
            {
                cookieGame.cookies = cookieStats.cookieScore;
                cookieGame.grandmaLvl = cookieStats.grandmaLevel;
                cookieGame.autoClickerLvl = cookieStats.autoClickerLevel;
                cookieGame.grandmaCost = cookieStats.grandmaCost;
                cookieGame.autoClickerCost = cookieStats.autoClickerCost;
            }
        }
    }
}
