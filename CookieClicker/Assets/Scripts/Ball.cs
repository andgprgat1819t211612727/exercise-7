﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    float x;
    float y;
    public float speed;
    public SaveUtility saveUtil;
    // Start is called before the first frame update
    void Start()
    {

        saveUtil.loadPosition();
    }

    // Update is called once per frame
    void Update()
    {
        x = Input.GetAxis("Horizontal");
        y = Input.GetAxis("Vertical");

        transform.position += new Vector3(x, y, 0) * (speed * Time.deltaTime);

        if(Input.GetKeyDown(KeyCode.R))
        {
            saveUtil.savePosition();
        }
    }
}
